/*
Copyright (C) 1994-1995 Apogee Software, Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include <stdarg.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>


#include <stdlib.h>
#include <sys/stat.h>
#include "modexlib.h"
//MED
#include "memcheck.h"
#include "rt_util.h"
#include "rt_net.h" // for GamePaused
#include "myprint.h"

static void StretchMemPicture ();
// GLOBAL VARIABLES

boolean StretchScreen=0;//bn�++
extern boolean iG_aimCross;
extern boolean sdl_fullscreen;
extern int iG_X_center;
extern int iG_Y_center;
char 	   *iG_buf_center;
  
int    linewidth;
//int    ylookup[MAXSCREENHEIGHT];
int    ylookup[600];//just set to max res
byte  *page1start;
byte  *page2start;
byte  *page3start;
int    screensize;
byte  *bufferofs;
byte  *displayofs;
boolean graphicsmode=false;
char        *bufofsTopLimit;
char        *bufofsBottomLimit;

void DrawCenterAim ();
void VH_UpdateScreen (void);


#include "SDL.h"

#ifndef STUB_FUNCTION

/* rt_def.h isn't included, so I just put this here... */
#if !defined(ANSIESC)
#define STUB_FUNCTION fprintf(stderr,"STUB: %s at " __FILE__ ", line %d, thread %d\n",__FUNCTION__,__LINE__,getpid())
#else
#define STUB_FUNCTION
#endif

#endif

/*
====================
=
= GraphicsMode
=
====================
*/
static SDL_Window  *sdl_window = NULL; 
static SDL_Renderer *sdl_renderer = NULL;
static SDL_Texture *sdl_texture = NULL;
static SDL_Texture *sdl_small_texture = NULL;

static byte *pixels = NULL;
SDL_Color palette[256]={};
uint32_t real_palette[256] = {};
SDL_Surface *sdl_surface = NULL;

SDL_Color* getPalette() {
    return &palette[0];
}

void setPalette(const SDL_Color* map, int count) {
    int format, h,w;
    SDL_PixelFormat pixelFormat = {};
    SDL_QueryTexture(sdl_texture, &format, NULL, &w, &h);
    pixelFormat.format = format;
    for(int i=0; i<256 && i<count; i++) {
        palette[i].r = map[i].r; // alpha
        palette[i].g = map[i].g; // blue
        palette[i].b = map[i].b;  // green
        palette[i].a = 0;
    }
    VH_UpdateScreen();
}

SDL_Window* getWindow() {
    return sdl_window;
}

void ToggleFullscreen() {
    if(sdl_fullscreen) {
        if(!SDL_SetWindowFullscreen(sdl_window, 0)) {
            sdl_fullscreen = 0;
        }
    } else {
        if(!SDL_SetWindowFullscreen(sdl_window, SDL_WINDOW_FULLSCREEN)) {
            sdl_fullscreen = 1;
        }
    }
}

void GraphicsMode ( void )
{
    Uint32 flags = 0;
    if (sdl_fullscreen)
        flags = SDL_WINDOW_FULLSCREEN;

	if (SDL_InitSubSystem (SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
	    Error ("Could not initialize SDL\n");
	}

    #if defined(PLATFORM_WIN32) || defined(PLATFORM_MACOSX)
        // FIXME: remove this.  --ryan.
        flags = SDL_FULLSCREEN;
        SDL_WM_GrabInput(SDL_GRAB_ON);
    #endif

    sdl_window = SDL_CreateWindow("Rise of the Triad",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            iGLOBAL_SCREENWIDTH,
            iGLOBAL_SCREENHEIGHT,
            flags
            );
    SDL_ShowCursor (SDL_DISABLE);
    SDL_CaptureMouse(SDL_TRUE);
    SDL_SetWindowGrab(sdl_window, SDL_TRUE);
    SDL_SetRelativeMouseMode(SDL_TRUE);
   
    sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED);
    if(sdl_renderer == NULL) {
        Error("Could not get renderer\n");
    }
    SDL_RenderClear(sdl_renderer);

    pixels = (byte*)calloc(iGLOBAL_SCREENWIDTH * iGLOBAL_SCREENHEIGHT, 1);
    if(pixels == NULL) {
        Error("Could not allocate pixel buffer\n");
    }

    sdl_texture = SDL_CreateTexture(sdl_renderer, 
             SDL_PIXELFORMAT_RGBA32,
            SDL_TEXTUREACCESS_STREAMING, iGLOBAL_SCREENWIDTH, iGLOBAL_SCREENHEIGHT);
    if(sdl_texture == NULL) {
        printf("SDL_Init failed: %s\n", SDL_GetError());
        Error("Could not allocate texture\n");

    }
    SDL_RenderSetLogicalSize(sdl_renderer, iGLOBAL_SCREENWIDTH, iGLOBAL_SCREENHEIGHT);
    sdl_small_texture = SDL_CreateTexture(sdl_renderer, 
             SDL_PIXELFORMAT_RGBA32,
            SDL_TEXTUREACCESS_STREAMING, 320, 200);
    if(sdl_small_texture == NULL) {
        printf("SDL_Init failed: %s\n", SDL_GetError());
        Error("Could not allocate texture\n");

    }
}

/*
====================
=
= SetTextMode
=
====================
*/
void SetTextMode ( void )
{
	if (SDL_WasInit(SDL_INIT_VIDEO) == SDL_INIT_VIDEO) {
        /*
         * Ysblokje
		if (sdl_surface != NULL) {
			SDL_FreeSurface(sdl_surface);
	
			sdl_surface = NULL;
		}*/
		SDL_QuitSubSystem (SDL_INIT_VIDEO);
	}
}

/*
====================
=
= TurnOffTextCursor
=
====================
*/
void TurnOffTextCursor ( void )
{
}

/*
====================
=
= WaitVBL
=
====================
*/
void WaitVBL( void )
{
	SDL_Delay (16667/1000);
}

/*
=======================
=
= VL_SetVGAPlaneMode
=
=======================
*/

void VL_SetVGAPlaneMode ( void )
{
   int i,offset;

    GraphicsMode();

//
// set up lookup tables
//
//bna--   linewidth = 320;
   linewidth = iGLOBAL_SCREENWIDTH;

   offset = 0;

   for (i=0;i<iGLOBAL_SCREENHEIGHT;i++)
      {
      ylookup[i]=offset;
      offset += linewidth;
      }

//    screensize=MAXSCREENHEIGHT*MAXSCREENWIDTH;
    screensize=iGLOBAL_SCREENHEIGHT*iGLOBAL_SCREENWIDTH;

    page1start=pixels;
    page2start=pixels;
    page3start=pixels;
    displayofs = page1start;
    bufferofs = page2start;

	iG_X_center = iGLOBAL_SCREENWIDTH / 2;
	iG_Y_center = (iGLOBAL_SCREENHEIGHT / 2)+10 ;//+10 = move aim down a bit

	iG_buf_center = bufferofs + (screensize/2);//(iG_Y_center*iGLOBAL_SCREENWIDTH);//+iG_X_center;

	bufofsTopLimit =  bufferofs + screensize - iGLOBAL_SCREENWIDTH;
	bufofsBottomLimit = bufferofs + iGLOBAL_SCREENWIDTH;

    // start stretched
    EnableScreenStretch();
    XFlipPage ();
}

/*
=======================
=
= VL_CopyPlanarPage
=
=======================
*/
void VL_CopyPlanarPage ( byte * src, byte * dest )
{
      memcpy(dest,src,screensize);
}

/*
=======================
=
= VL_CopyPlanarPageToMemory
=
=======================
*/
void VL_CopyPlanarPageToMemory ( byte * src, byte * dest )
{
      memcpy(dest,src,screensize);
}

/*
=======================
=
= VL_CopyBufferToAll
=
=======================
*/
void VL_CopyBufferToAll ( byte *buffer )
{
    int plane;                                                 
    for (plane=0;plane<4;plane++)                              
    {                                                       
        VGAREADMAP(plane);                                      
        VGAWRITEMAP(plane);                                     
        if (page1start!=buffer)                                 
            memcpy((byte *)page1start,(byte *)buffer,screensize);
        if (page2start!=buffer)                                 
            memcpy((byte *)page2start,(byte *)buffer,screensize);
        if (page3start!=buffer)                                 
            memcpy((byte *)page3start,(byte *)buffer,screensize);
    }                                                       
}

/*
=======================
=
= VL_CopyDisplayToHidden
=
=======================
*/
void VL_CopyDisplayToHidden ( void )
{
   VL_CopyBufferToAll ( displayofs );
}

/*
=================
=
= VL_ClearBuffer
=
= Fill the entire video buffer with a given color
=
=================
*/

void VL_ClearBuffer (byte *buf, byte color)
{
  memset((byte *)buf,color,screensize);
}

/*
=================
=
= VL_ClearVideo
=
= Fill the entire video buffer with a given color
=
=================
*/

void VL_ClearVideo (byte color)
{
  memset (pixels, color, iGLOBAL_SCREENWIDTH*iGLOBAL_SCREENHEIGHT);
}

/*
=================
=
= VL_DePlaneVGA
=
=================
*/

void VL_DePlaneVGA (void)
{
}


void PrepareSurface() {
// 	if (StretchScreen){//bna++
//		StretchMemPicture ();
//	}else{
//		DrawCenterAim ();
//	}
//    SDL_Surface *dest = SDL_GetWindowSurface(sdl_window);
//    SDL_BlitSurface(sdl_surface, NULL, dest, NULL);
//    SDL_UpdateWindowSurface(sdl_window);
//    Ysblokje
//
    SDL_Texture *dst_texture = sdl_texture;
    int max_y = iGLOBAL_SCREENHEIGHT;
    int max_x = iGLOBAL_SCREENWIDTH;
    int skip_x = 0;
    if(StretchScreen) {
        dst_texture = sdl_small_texture;
        max_y = 200;
        max_x = 320;
        skip_x = 320;
    } else {
		DrawCenterAim ();
    }

    uint32_t *texturepixels;
    int pitch;
    SDL_RenderClear(sdl_renderer);
    if(!dst_texture)
         return;
    SDL_LockTexture(dst_texture, NULL, (void**)&texturepixels, &pitch);
    int i, j, ny, dy;
    for(int y=0; y < max_y; y++) {
        ny = y * iGLOBAL_SCREENWIDTH;
        dy = y * max_x;
        for(int x=0; x < max_x; x++) {
            i = ny + x;
            j = dy + x;
            texturepixels[j] = *(uint32_t*)&(palette[pixels[i]]);
        }
    }
    // stuff
    SDL_UnlockTexture(dst_texture);
    SDL_SetRenderTarget(sdl_renderer, NULL);
    SDL_RenderCopy(sdl_renderer, dst_texture, NULL, NULL);
    SDL_RenderPresent(sdl_renderer);
}

/* C version of rt_vh_a.asm */

void VH_UpdateScreen (void)
{ 	
    PrepareSurface();

//	SDL_UpdateRect (SDL_GetVideoSurface (), 0, 0, 0, 0);
}


/*
=================
=
= XFlipPage
=
=================
*/

void XFlipPage ( void )
{
   PrepareSurface();
//   SDL_UpdateRect (sdl_surface, 0, 0, 0, 0);
}



void EnableScreenStretch(void)
{
#if 0   
   int i,offset;
   if (iGLOBAL_SCREENWIDTH <= 320 || StretchScreen) return;
   
   if (unstretch_sdl_surface == NULL)
   {
      /* should really be just 320x200, but there is code all over the
         places which crashes then */
      unstretch_sdl_surface = SDL_CreateRGBSurface(SDL_SWSURFACE,
         iGLOBAL_SCREENWIDTH, iGLOBAL_SCREENHEIGHT, 8, 0, 0, 0, 0);
   }
	
   displayofs = unstretch_sdl_surface->pixels +
	(displayofs - (byte *)sdl_surface->pixels);
   bufferofs  = unstretch_sdl_surface->pixels;
   page1start = unstretch_sdl_surface->pixels;
   page2start = unstretch_sdl_surface->pixels;
   page3start = unstretch_sdl_surface->pixels;
#endif
   StretchScreen = 1;	
}

void DisableScreenStretch(void)
{
   if (iGLOBAL_SCREENWIDTH <= 320 || !StretchScreen) return;
#if 0
   displayofs = sdl_surface->pixels +
	(displayofs - (byte *)unstretch_sdl_surface->pixels);
   bufferofs  = sdl_surface->pixels;
   page1start = sdl_surface->pixels;
   page2start = sdl_surface->pixels;
   page3start = sdl_surface->pixels;
#endif
   StretchScreen = 0;
}

#if 0
// bna section -------------------------------------------
static void StretchMemPicture ()
{
  SDL_Rect src;
  SDL_Rect dest;
	
  src.x = 0;
  src.y = 0;
  src.w = 320;
  src.h = 200;
  
  dest.x = 0;
  dest.y = 0;
  dest.w = iGLOBAL_SCREENWIDTH;
  dest.h = iGLOBAL_SCREENHEIGHT;
  SDL_SoftStretch(unstretch_sdl_surface, &src, sdl_surface, &dest);
}
#endif
// bna function added start
extern	boolean ingame;
int		iG_playerTilt;

void DrawCenterAim ()
{
	int x;

	int percenthealth = (locplayerstate->health * 10) / MaxHitpointsForCharacter(locplayerstate);
	int color = percenthealth < 3 ? egacolor[RED] : percenthealth < 4 ? egacolor[YELLOW] : egacolor[GREEN];

	if (iG_aimCross && !GamePaused){
		if (( ingame == true )&&(iGLOBAL_SCREENWIDTH>320)){
			  if ((iG_playerTilt <0 )||(iG_playerTilt >iGLOBAL_SCREENHEIGHT/2)){
					iG_playerTilt = -(2048 - iG_playerTilt);
			  }
			  if (iGLOBAL_SCREENWIDTH == 640){ x = iG_playerTilt;iG_playerTilt=x/2; }
			  iG_buf_center = bufferofs + ((iG_Y_center-iG_playerTilt)*iGLOBAL_SCREENWIDTH);//+iG_X_center;

			  for (x=iG_X_center-10;x<=iG_X_center-4;x++){
				  if ((iG_buf_center+x < bufofsTopLimit)&&(iG_buf_center+x > bufofsBottomLimit)){
					 *(iG_buf_center+x) = color;
				  }
			  }
			  for (x=iG_X_center+4;x<=iG_X_center+10;x++){
				  if ((iG_buf_center+x < bufofsTopLimit)&&(iG_buf_center+x > bufofsBottomLimit)){
					 *(iG_buf_center+x) = color;
				  }
			  }
			  for (x=10;x>=4;x--){
				  if (((iG_buf_center-(x*iGLOBAL_SCREENWIDTH)+iG_X_center) < bufofsTopLimit)&&((iG_buf_center-(x*iGLOBAL_SCREENWIDTH)+iG_X_center) > bufofsBottomLimit)){
					 *(iG_buf_center-(x*iGLOBAL_SCREENWIDTH)+iG_X_center) = color;
				  }
			  }
			  for (x=4;x<=10;x++){
				  if (((iG_buf_center+(x*iGLOBAL_SCREENWIDTH)+iG_X_center) < bufofsTopLimit)&&((iG_buf_center+(x*iGLOBAL_SCREENWIDTH)+iG_X_center) > bufofsBottomLimit)){
					 *(iG_buf_center+(x*iGLOBAL_SCREENWIDTH)+iG_X_center) = color;
				  }
			  }
		}
	}
}
// bna function added end




// bna section -------------------------------------------



